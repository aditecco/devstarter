DevStarter
==========

By <http://ditecco.me>


A full-featured, gulp-based
front-end boilerplate.


### Featuring:

#### Markup processing & templating:

- [gulp-nunjucks](https://github.com/sindresorhus/gulp-nunjucks)
- [gulp-htmlmin](https://github.com/jonschlinkert/gulp-htmlmin)
 

#### CSS authoring & automation:

- [gulp-sass](https://github.com/dlmanning/gulp-sass)
- [gulp-postcss](https://github.com/postcss/gulp-postcss)
- [postcss-preset-env](https://github.com/csstools/postcss-preset-env)
- [autoprefixer](https://github.com/postcss/autoprefixer)
- [postcss-critical-css](https://github.com/zgreen/postcss-critical-css)
- [oldie](https://github.com/jonathantneal/oldie)
- [cssnano](https://cssnano.co)


#### Modern JS:

- [gulp-babel](https://github.com/babel/gulp-babel)


#### Utilities

- [gulp-uglify](https://github.com/terinjokes/gulp-uglify/)
- [gulp-imagemin](https://github.com/sindresorhus/gulp-imagemin)
- [gulp-sourcemaps](https://github.com/gulp-sourcemaps/gulp-sourcemaps)
- [browsersync](https://www.browsersync.io/docs/gulp)
- [gulp-debug](https://github.com/sindresorhus/gulp-debug)
- [gulp-rename](https://github.com/hparra/gulp-rename)
