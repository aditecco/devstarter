
/* ---------------------------------

DevStarter by http://ditecco.me

A full-featured, gulp-based
front-end boilerplate.

--------------------------------- */

 
/**
 * Dependency reference:
 * 
 * https://github.com/sindresorhus/gulp-nunjucks
 * https://github.com/jonschlinkert/gulp-htmlmin
 * 
 * https://github.com/dlmanning/gulp-sass
 * https://github.com/postcss/gulp-postcss
 * https://github.com/csstools/postcss-preset-env
 * https://github.com/postcss/autoprefixer
 *  opts: `npx autoprefixer --info`
 * https://github.com/zgreen/postcss-critical-css
 * https://github.com/jonathantneal/oldie
 * https://cssnano.co
 * 
 * https://github.com/terinjokes/gulp-uglify/
 * https://github.com/sindresorhus/gulp-imagemin
 * https://github.com/gulp-sourcemaps/gulp-sourcemaps
 * https://www.browsersync.io/docs/gulp
 *  opts: https://www.browsersync.io/docs/options
 * https://github.com/sindresorhus/gulp-debug
 * https://github.com/hparra/gulp-rename
 */

import gulp from 'gulp';
import babel from 'gulp-babel';
import nunjucks from "gulp-nunjucks";
import htmlmin from "gulp-htmlmin";
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import postcssPresetEnv from "postcss-preset-env";
import critical from 'postcss-critical-css';
import autoprefixer from 'autoprefixer';
import oldie from "oldie";
import cssnano from "cssnano";
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import imagemin from 'gulp-imagemin';
import sourcemaps from "gulp-sourcemaps";
import debug from 'gulp-debug';
import rename from 'gulp-rename';
import browserSync from "browser-sync";
import del from 'del';


/* ---------------------------------
env
--------------------------------- */

/**
 * source, public labels:
 * computed property names let us
 * freely change the input/output
 * directory labels (default: src, build)
 */

const _in = 'src';
const _out = 'build';

const paths = {
  markup: {
    [_in]: `${_in}/markup/**/*.{html,njk}`,
    [_out]: `${_out}/`
  },
  assets: {
    [_in]: `assets/**/*.png`,
    [_out]: `${_out}/assets/`
  },
  styles: {
    [_in]: `${_in}/styles/**/*.{css,scss}`,
    [_out]: `${_out}/styles/`
  },
  scripts: {
    [_in]: `${_in}/scripts/**/*.js`,
    [_out]: `${_out}/scripts/`
  }
};

// gulp API shortcuts
const {
  series,
  parallel,
  src,
  dest,
  watch
} = gulp;

// utils
const server = browserSync.create();


/* ---------------------------------
tasks
--------------------------------- */


/**
 * Cleanup
 * Deletes the contents of the build directory
 */

export const cleanup = () => del(`${_out}/*`);


/**
 * Markup
 * 
 * Processes markup, with support for:
 *  - includes
 *  - minification
 */

export function markup() {
  console.info('>>> processing markup…')

  const opts = {
    minifier: {
      collapseWhitespace: true,
      maxLineLength: 80,
      quoteCharacter: '\"',
      removeComments: true,
      removeEmptyAttributes: true,
      minifyCSS: true,
      minifyJS: true,
    }
  }

  return src(paths['markup'][_in])
    .pipe(nunjucks.compile())
    .pipe(htmlmin(opts.minifier))
    .pipe(rename({ extname: '.html' }))
    .pipe(debug({title: 'markupLog:'}))
    .pipe(dest(paths['markup'][_out]))
}


/**
 * Styles
 * 
 * Processes stylesheets, with support for:
 *  - Sass
 *  - autoprefixing
 *  - legacy IE hacks
 *  - critical CSS
 *  - minification
 *  - sourcemaps
 */

export function styles() {
  console.info('>>> processing styles…')

  const plugins = [
    autoprefixer({ grid: 'autoplace' }),
    cssnano({ preset: 'default' }),
    oldie(),
    critical({
      outputPath: `${paths['styles'][_out]}`,
      outputDest: 'critical.min.css',
      preserve: false,
    }),
  ];

  return src(paths['styles'][_in])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(plugins, {}))
    .pipe(sourcemaps.write('./'))
    .pipe(rename({
      basename: 'main',
      suffix: '.min'
    }))
    .pipe(debug({title: 'stylesLog:'}))
    .pipe(dest(paths['styles'][_out]))
}


/**
 * Scripts task
 * 
 * Processes scripts, with support for:
 *  - ES6 syntax
 *  - concatenation
 *  - minification
 *  - sourcemaps
 */

export function scripts() {
  console.info('>>> processing scripts…')

  return src(paths['scripts'][_in])
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(uglify())
    .pipe(concat('bundle.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(debug({title: 'scriptsLog:'}))
    .pipe(dest(paths['scripts'][_out]))
}


/**
 * Assets task
 * 
 * Processes assets, with support for:
 *  - Image compression
 */

export function assets() {
  console.info('>>> processing assets…')

  return src(paths['assets'][_in])
    .pipe(imagemin({
      verbose: true,
    }))
    .pipe(dest(paths['assets'][_out]))
}


/**
 * Watcher
 * 
 * Watches all source files for changes
 */

function watcher() {
  const resetAssets = () => del([`${_out}/assets/*`]);
  
  watch(paths['markup'][_in], gulp.series(markup, reload));
  watch(paths['assets'][_in], gulp.series(resetAssets, assets, reload));
  watch(paths['styles'][_in], gulp.series(styles, reload));
  watch(paths['scripts'][_in], gulp.series(scripts, reload));
}


/**
 * serve
 * 
 * Starts a BrowserSync instance
 */

export function serve(done) {
  server.init(
    {
      browser: "google chrome",
      open: false,
      server: { baseDir: 'build', }
    }
  );

  done()
}


// reloads the BrowserSync instance
const reload = (done) => { server.reload(); done(); }


/**
 * default
 * 
 * Runs all development tasks in series
 */

const dev = series(
  cleanup,
  markup,
  assets,
  styles,
  scripts,
  serve,
  watcher
);

export default dev;
